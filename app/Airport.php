<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Airport extends Model
{
    protected $table = 'airports';
    protected $fillable = ['name', 'code', 'state'];

    public function departures(){
        return $this->hasMany('App\Flight','airport_departure_id');
    }

    public function arrivals(){
        return $this->hasMany('App\Flight', 'airport_arrival_id');
    }
}
