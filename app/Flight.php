<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Flight extends Model
{
    protected $table = 'flights';
    protected $fillable = ['flightCode', 'airport_departure_id', 'airport_arrival_id', 'departureTime', 'arrivalTime'];

    public function departure()
    {
        return $this->belongsTo('App\Airport', 'airport_departure_id');
    }

    public function arrival()
    {
        return $this->belongsTo('App\Airport', 'airport_arrival_id');
    }

    public function passenger()
    {
        return $this->belongsToMany('App\Passenger', 'flight_passenger');
    }
}
