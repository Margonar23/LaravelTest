<?php
/**
 * Created by PhpStorm.
 * User: margonar
 * Date: 04/01/18
 * Time: 15.59
 */

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Airport;

class AirportController extends Controller
{

    /**
     * AirportController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function airport()
    {
        $airports = Airport::all();
        return view('airport', ['airports' => $airports]);
    }

    public function add(Request $request){
        $airport = new Airport();
        $airport->name = $request->name;
        $airport->code = $request->code;
        $airport->state = $request->state;


        $airport->save();

        return redirect('airport');
    }
}