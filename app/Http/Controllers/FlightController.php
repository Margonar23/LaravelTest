<?php
/**
 * Created by PhpStorm.
 * User: margonar
 * Date: 04/01/18
 * Time: 15.59
 */

namespace App\Http\Controllers;


use App\Airport;
use Illuminate\Http\Request;
use App\Flight;

class FlightController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function flight()
    {
        $flights = Flight::all();
        $airports = Airport::all();
        return view('flight', ['flights' => $flights, 'airports' => $airports]);
    }

    public function store(Request $request)
    {
        $flight = new Flight();
        $flight->flightCode = $request->code;
        $flight->airport_departure_id = $request->departure;
        $flight->airport_arrival_id = $request->arrival;
        $flight->departureTime = $request->departureTime;
        $flight->arrivalTime = $request->arrivalTime;

        $flight->save();

        return redirect('flight');
    }

    public function view($id)
    {
        $flight = Flight::find($id);
        return view('flightDetail', ['flight' => $flight]);
    }
}