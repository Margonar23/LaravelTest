<?php
/**
 * Created by PhpStorm.
 * User: margonar
 * Date: 05/01/18
 * Time: 10.18
 */

namespace App\Http\Controllers;

use App\Passenger;
use App\Location;
use Illuminate\Http\Request;

class PassengerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function passenger()
    {
        $passenger = Passenger::all();
        return view('passenger', ['passenger' => $passenger]);
    }

    public function store(Request $request)
    {
        $location = new Location();
        $location->route = $request->route;
        $location->number = $request->number;
        $location->nap = $request->nap;
        $location->city = $request->city;
        $location->state = $request->state;

        $location->save();

        $passenger = new Passenger();
        $passenger->firstname = $request->firstname;
        $passenger->lastname = $request->lastname;
        $passenger->email = $request->email;
        $passenger->locations_id = $location->id;

        $passenger->save();

        return redirect()->back()->with('status', 'Passenger added')->with('code','alert-success');

    }
}