<?php
/**
 * Created by PhpStorm.
 * User: margonar
 * Date: 04/01/18
 * Time: 15.59
 */

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Passenger;
use App\Flight;

class TicketController extends Controller
{

    /**
     * AirportController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function ticket()
    {
        $passenger = Passenger::all();
        $flights = Flight::all();
        return view('ticket', ['passenger' => $passenger, 'flights' => $flights]);
    }

    public function store(Request $request)
    {
        $passenger = Passenger::find($request->passenger);
        $flight = Flight::find($request->flight);

        if ($flight->passenger->contains($passenger)){
            return redirect()->back()->with('status', 'Ticket already bought')->with('code','alert-warning');
        }

        $passenger->flight()->save($flight);
        return redirect()->back()->with('status','Ticket bought successfully')->with('code','alert-success');
    }

}