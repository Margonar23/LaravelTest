<?php
/**
 * Created by PhpStorm.
 * User: margonar
 * Date: 04/01/18
 * Time: 13.51
 */

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\User;

class UserController extends Controller
{


    /**
     * UserController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function profile(){
        $user = Auth::user();
        return view('profile', ['auth' => $user]);
    }


    public function logout(){
        Auth::logout();
        return view('welcome');
    }
}