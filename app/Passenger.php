<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Passenger extends Model
{
    protected $table = 'passenger';
    protected $fillable = ['lastname', 'firstname', 'email', 'locations_id'];


    public function location()
    {
        return $this->belongsTo('App\Location', 'locations_id');
    }

    public function flight()
    {
        return $this->belongsToMany('App\Flight', 'flight_passenger');
    }




}
