$("#alertMessage").hide();

$('document').ready(function () {
    if ($("#sessionStatus").val() != "") {
        $("#alertMessage").fadeTo(2000, 500).slideUp(800, function () {
            $("#alertMessage").slideUp(1000);
        });
    }
});