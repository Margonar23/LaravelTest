@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <form class="form-horizontal" method="POST" action="{{ route('addAirport') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <label class="col-md-4 control-label">Airport name</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" id="name" name="name" min="1" max="100">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Airport code</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" id="code" name="code" min="1" max="5">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Airport state</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" id="state" name="state" min="1" max="100">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Create
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-lg-12">
                @if (count($airports) > 0)
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Code</th>
                            <th>Name</th>
                            <th>State</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($airports as $airport)
                                <tr>
                                    <td>{{ $airport->id }}</td>
                                    <td>{{ $airport->code }}</td>
                                    <td>{{ $airport->name }}</td>
                                    <td>{{ $airport->state }}</td>
                                </tr>
                                @endforeach
                        </tbody>
                    </table>
                @else
                    <div class="alert alert-info">No airports</div>
                @endif
            </div>
        </div>
    </div>
@endsection
