@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <form class="form-horizontal" method="POST" action="{{ route('addFlight') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <label class="col-md-4 control-label">Flight code</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" id="code" name="code" min="1" max="5">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Airport departure</label>
                        <div class="col-md-6">
                            <select class="form-control" name="departure">
                                @foreach ($airports as $airport)
                                    <option value="{{ $airport->id }}"> ({{ $airport->code }}
                                        ) {{ $airport->name }} </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Departure time</label>
                        <div class="col-md-6">
                            <input type="datetime-local" class="form-control" name="departureTime">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Airport arrival</label>
                        <div class="col-md-6">
                            <select class="form-control" name="arrival">
                                @foreach ($airports as $airport)
                                    <option value="{{ $airport->id }}"> ({{ $airport->code }}
                                        ) {{ $airport->name }} </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Arrival time</label>
                        <div class="col-md-6">
                            <input type="datetime-local" class="form-control" name="arrivalTime">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Create
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-lg-12">
                @if (count($flights) > 0)
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Code</th>
                            <th>Departure</th>
                            <th>Arrival</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($flights as $flight)
                            <tr>
                                <td>{{ $flight->id }}</td>
                                <td>{{ $flight->flightCode }}</td>
                                <td>{{ $flight->departureTime }} {{ $flight->departure->code }}</td>
                                <td>{{ $flight->arrivalTime }} {{ $flight->arrival->code }}</td>
                                <td><a href="{{ route('viewFlight', ['id' => $flight->id]) }}"><i class="glyphicon glyphicon-info-sign"></i> </a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @else
                    <div class="alert alert-info">No airports</div>
                @endif
            </div>
        </div>
    </div>
@endsection
