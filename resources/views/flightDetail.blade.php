@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-11">
                <table class="table table-bordered">
                    <tr>
                        <th colspan="3">{{ $flight->flightCode }}</th>
                    </tr>
                    <tr>
                        <th>From</th>
                        <th>{{ $flight->departure->code }}</th>
                        <th>{{ $flight->departure->name }}</th>
                    </tr>
                    <tr>
                        <th><i class="glyphicon glyphicon-time"></i></th>
                        <th colspan="2">{{ $flight->departureTime }}</th>
                    </tr>
                    <tr>
                        <th>To</th>
                        <th>{{ $flight->arrival->code }}</th>
                        <th>{{ $flight->arrival->name }}</th>
                    </tr>
                    <tr>
                        <th><i class="glyphicon glyphicon-time"></i></th>
                        <th colspan="2">{{ $flight->arrivalTime }}</th>
                    </tr>
                </table>
            </div>
            <div class="col-lg-11">
                <h3>Passenger list</h3>
                @if(count($flight->passenger) > 0)
                    <table class="table table-responsive">
                        @foreach($flight->passenger as $passenger)
                            <tr>
                                <td>{{ $passenger->lastname }}, {{ $passenger->firstname}}</td>
                                <td>{{ $passenger->email }}</td>
                            </tr>
                        @endforeach
                    </table>
                @else
                    <div class="alert alert-info">No passenger on this flight</div>
                @endif
            </div>
        </div>
    </div>
@endsection
