@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>

                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        Welcome {{$auth->name}}
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Email</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($usersList as $user)
                        <tr>
                            <td>{{ $user->id }}</td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="col-lg-12">
                @foreach($flights as $flight)
                    <table class="table table-bordered">
                        <tr style="background-color: #5bc0de">
                            <th colspan="3">{{ $flight->flightCode }}</th>
                        </tr>
                        <tr>
                            <th>From</th>
                            <th>{{ $flight->departure->code }}</th>
                            <th>{{ $flight->departure->name }}</th>
                        </tr>
                        <tr>
                            <th><i class="glyphicon glyphicon-time"></i></th>
                            <th colspan="2">{{ $flight->departureTime }}</th>
                        </tr>
                        <tr>
                            <th>To</th>
                            <th>{{ $flight->arrival->code }}</th>
                            <th>{{ $flight->arrival->name }}</th>
                        </tr>
                        <tr>
                            <th><i class="glyphicon glyphicon-time"></i></th>
                            <th colspan="2">{{ $flight->arrivalTime }}</th>
                        </tr>
                        <tr>
                            <th colspan="4" style="background-color: #d7ebf6">Passenger</th>
                        </tr>
                        @foreach($flight->passenger as $passenger)
                            <tr>
                                <td colspan="2">{{ $passenger->firstname }} {{ $passenger->lastname }}</td>
                                <td colspan="2">{{ $passenger->email }}</td>
                            </tr>
                        @endforeach
                    </table>
                @endforeach
            </div>
        </div>
    </div>
@endsection
