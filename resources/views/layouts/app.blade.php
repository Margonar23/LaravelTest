<html>
<head>
    <title>@yield('pageTitle')</title>
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('css/jumbotron-narrow.css') }}" rel="stylesheet">


</head>
<body>

<div class="container">
    <div class="header clearfix">
        <nav>
            <ul class="nav nav-pills pull-right">
                <li role="presentation" @if (Route::current()->getName() == 'home') class="active" @endif>
                    <a href="{{ route('home') }}">Home</a>
                </li>
                <li role="presentation" @if (Route::current()->getName() == 'airport') class="active" @endif>
                    <a href="{{ route('airport') }}">Airport</a>
                </li>
                <li role="presentation" @if (Route::current()->getName() == 'flight') class="active" @endif>
                    <a href="{{ route('flight') }}">Flight</a>
                </li>
                <li role="presentation" @if (Route::current()->getName() == 'passenger') class="active" @endif>
                    <a href="{{ route('passenger') }}">Passenger</a>
                </li>
                <li role="presentation" @if (Route::current()->getName() == 'ticket') class="active" @endif>
                    <a href="{{ route('ticket') }}">Ticket</a>
                </li>
                <li role="presentation" @if (Route::current()->getName() == 'profile') class="active" @endif>
                    <a href="{{ route('profile') }}"><i class="glyphicon glyphicon-user"></i></a>
                </li>

                <li role="presentation">
                    <a href="{{ Route('logout') }}"><i class="glyphicon glyphicon-log-out"></i></a>
                </li>

            </ul>
        </nav>
        <h3 class="text-muted">Project Name</h3>
    </div>


    @yield('content')


    <footer class="footer">
        <p>&copy; 2016 Company, Inc.</p>
    </footer>
</div>

<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/bootstrap.js') }}"></script>
<script src="{{ asset('js/npm.js') }}"></script>

@stack('scripts')
</body>
</html>