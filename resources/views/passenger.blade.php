@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">

            <input type="hidden" value="{{ session('status') }}" id="sessionStatus" />
            <div class="alert {{ session('code') }} alert-block" id="alertMessage">
                <button type="button" class="close" data-dismiss="alert">×</button>
                {{ session('status') }}
            </div>

            <div class="col-lg-12">
                <form class="form-horizontal" method="POST" action="{{ route('addPassenger') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <label class="col-md-4 control-label">First Name</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="firstname" min="1" max="15">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Last Name</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="lastname" min="1" max="25">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">E-Mail</label>
                        <div class="col-md-6">
                            <input type="email" class="form-control" name="email">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Route</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="route">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Number</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="number">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">NAP</label>
                        <div class="col-md-6">
                            <input type="number" class="form-control" name="nap">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">City</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="city">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">State</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="state">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Create
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-lg-12">
                @if (count($passenger) > 0)
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Address</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($passenger as $p)
                            <tr>
                                <td>{{ $p->id }}</td>
                                <td>{{ $p->firstname }} {{ $p->lastname }}</td>
                                <td>{{ $p->email }}</td>
                                <td>{{ $p->location->route }} {{ $p->location->number }},
                                    <br>{{ $p->location->nap }} {{ $p->location->city }} {{ $p->location->state }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @else
                    <div class="alert alert-info">No passenger</div>
                @endif
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script src="{{ asset('js/alert.js') }}"></script>
@endpush
