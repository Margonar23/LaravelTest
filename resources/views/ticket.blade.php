@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <h1>Ticket</h1>
            
            <input type="hidden" value="{{ session('status') }}" id="sessionStatus" />
            <div class="alert {{ session('code') }} alert-block" id="alertMessage">
                <button type="button" class="close" data-dismiss="alert">×</button>
                {{ session('status') }}
            </div>


            <div class="col-lg-12">
                <form class="form-horizontal" method="POST" action="{{ route('addTicket') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <label class="col-md-2 control-label">Passenger</label>
                        <div class="col-md-8">
                            <select class="form-control" name="passenger">
                                @foreach ($passenger as $p)
                                    <option value="{{ $p->id }}"> {{ $p->fistname }} {{ $p->lastname }} </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Flight</label>
                        <div class="col-md-8">
                            <select class="form-control" name="flight">
                                @foreach ($flights as $flight)
                                    <option value="{{ $flight->id }}">
                                        ({{ $flight->flightCode }}) {{ $flight->departure->code }}
                                        to
                                        {{ $flight->arrival->code }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Create
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script src="{{ asset('js/alert.js') }}"></script>
@endpush