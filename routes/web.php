<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/profile', 'UserController@profile')->name('profile');
Route::get('/logout', 'UserController@logout')->name('logout');

Route::get('/airport', 'AirportController@airport')->name('airport');
Route::post('/airport/store', 'AirportController@add')->name('addAirport');

Route::get('/flight', 'FlightController@flight')->name('flight');
Route::post('/flight/store', 'FlightController@store')->name('addFlight');
Route::get('/flight/{id}', 'FlightController@view')->name('viewFlight');

Route::get('/passenger', 'PassengerController@passenger')->name('passenger');
Route::post('/passenger/store', 'PassengerController@store')->name('addPassenger');

Route::get('/ticket', 'TicketController@ticket')->name('ticket');
Route::post('/ticket/store', 'TicketController@store')->name('addTicket');
